import requests

from pydantic import BaseModel

from config import vk_token, vk_version


def get_posts(domain: str, count_posts: int) -> list:
    response = requests.get(f'''https://api.vk.com/method/wall.get?
                            domain={domain}&count={count_posts}&
                            access_token={vk_token}&v={vk_version}''')
    response = response.json()['response']['items']
    content = []
    for items in response:
        if items['marked_as_ads'] == 1:
            continue
        else:
            content.append(items)
    return content


class VKPost(BaseModel):
    id: int
    date: int
    text: str


def get_vk(domain: str, count_posts: int) -> list:
    result = []
    content = get_posts(domain, count_posts)
    for item in content:
        post = VKPost(**item)
        result.append(post)
    return result
