from slixmpp import ClientXMPP

from vk_parser import get_vk
from db_api import DataBase
from config import JID, JID_password


class Bot(ClientXMPP):

    def __init__(self, jid: str, password: str) -> None:
        ClientXMPP.__init__(self, jid, password)
        self.add_event_handler('session_start', self.session_start)
        self.add_event_handler('message', self.message_handler)

    def session_start(self, event):
        self.send_presence()
        self.get_roster()
        print('session started')

    def update(self, msg, sender: str, count_posts: int = 10) -> None:
        user = DataBase(sender)
        links = user.get_links()
        for link in links:
            items_list = get_vk(link, count_posts)
            try:
                for items in items_list:
                    msg.reply(f"___\n*{link}*:\n{items.text}\n___").send()
            except Exception:
                msg.reply(f"не могу получить данные из {link}")

    def undefined_command(self, msg, sender: str) -> None:
        print(msg)
        msg.reply(f"Привет, {sender}, меня зовут Vinni."
                  " Я могу брать посты из ВК и присылать их тебе в личку."
                  " Сейчас тебе доступны следующие функции:"
                  "\n /add - добавить одну или несколько подписок"
                  "\n /up - получить новые посты"
                  "\n /rm - удалить одну или несколько подписок"
                  "\n /show - показать все подписки"
                  "\n /get - получить посты из паблика").send()

    def add(self, msg, sender: str) -> None:
        user = DataBase(sender)
        message_body = msg['body'][5:].strip().split()
        if len(message_body) == 0:
            msg.reply('после /add должна быть ссылка').send()
        else:
            for i in message_body:
                if i.startswith('https://vk.com/'):
                    domain = i[15:]
                else:
                    domain = i
                user.add_link(domain)
                msg.reply(f'добавлен {domain}').send()

    def remove(self, msg, sender: str) -> None:
        user = DataBase(sender)
        message_body = msg['body'].strip().split()
        if len(message_body) == 1:
            msg.reply('после /rm должна быть ссылка или '
                      '-all для удаления всех подписок').send()
        elif message_body[1] == '-all':
            user.remove_table()
            msg.reply(f'все подписки {sender} были удалены').send()
        else:
            message_body = message_body[1:]
            for link in message_body:
                user.remove_link(link)
                msg.reply(f'{link} удален').send()

    def show(self, msg, sender: str) -> None:
        user = DataBase(sender)
        links = user.get_links()
        if len(links) > 0:
            msg.reply('\n'.join(links)).send()
        else:
            msg.reply(f'у {sender} нет подписок').send()

    def get(self, msg) -> None:
        message_body = msg['body'][5:].strip().split()
        if len(message_body) == 2:
            domain = message_body[0]
            count_posts = int(message_body[1])
            items_list = get_vk(domain, count_posts)
            try:
                for items in items_list:
                    msg.reply(f"___\n*{domain}*:\n{items.text}\n___").send()
            except Exception:
                msg.reply(f"не могу получить данные из {domain}")
        else:
            msg.reply('введите ссылку и количество постов после /get').send()

    def message_handler(self, msg) -> None:
        sender = str(msg['from'])
        if '/' in sender:
            sender = sender[:sender.index('/')]

        if msg['body'].startswith('/add'):
            self.add(msg, sender)

        elif msg['body'].startswith('/up'):
            self.update(msg, sender)

        elif msg['body'].startswith('/rm'):
            self.remove(msg, sender)

        elif msg['body'].startswith('/show'):
            self.show(msg, sender)

        elif msg['body'].startswith('/get'):
            self.get(msg)

        else:
            self.undefined_command(msg, sender)


def main():
    vinni = Bot(JID, JID_password)
    vinni.connect()
    vinni.process()


if __name__ == '__main__':
    main()
